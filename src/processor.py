
""" Author: Aleck MacNally
    Purpose: Exposes the Processor Class for processing a video frame by frame.
    Currently disposes of audio."""

import statistics
import time

import cv2 as cv
from tqdm import tqdm
from matplotlib import pyplot as plt

class Processor:
    """ Class for taking images one-by-one from a video stream
        and passing them to a set of prescribed plugins."""

    # pylint: disable=too-many-instance-attributes
    # pylint: disable-msg=too-many-arguments
    # Nine is reasonable in this case.

    def __init__(self, device, name, play, frame_memory, frame_skip, hard_skip, plugins: list = None,
                 fourcc="MJPG", progress_indicator=True, length=-1, file_name=None):

        """ Base constructor for Processor. Should only be accessed through process_file
            or process_camera."""

        self._plugins = plugins if plugins is not None else list()
        self._video_cap = cv.VideoCapture(device)
        self._device = device
        self._play = play
        self._memory = frame_memory
        self._skip = frame_skip
        self._hard_skip = hard_skip
        fps = self._video_cap.get(cv.CAP_PROP_FPS)

        if type(device) is int:
            self._progress = False
        else:
            self._progress = progress_indicator

        if type(device) is not int:
            self._length = int(self._video_cap.get(cv.CAP_PROP_FRAME_COUNT))
        else:
            self._length = -1

        self._player = None
        self._skip_memory = 0
        self._hard_skip_memory = 0
        plt.axis('off')

        frame_size = (int(self._video_cap.get(3)), int(self._video_cap.get(4)))
        video_writer = cv.VideoWriter_fourcc(*fourcc)
        self._video_out = cv.VideoWriter(name, video_writer, fps, frame_size)


    def process_frame(self):
        """ Function which takes a frame from from the video source and
            applies each of the prescribed plugins to it, in the order
            specified on the command line."""

        ret, frame = self._video_cap.read()

        if not ret:
            current_frame = self._video_cap.get(cv.CAP_PROP_POS_FRAMES)
            if current_frame < self._length:
                self._video_cap.set(cv.CAP_PROP_POS_FRAMES, current_frame + 1)
                return True
            else:
                return False

        skip = not self._skip_memory_proc()
        hard_skip = not self._hard_skip_memory_proc()
        for plugin in self._plugins:
            plug = plugin(self._device, self._memory, frame)

            if hard_skip:
                plug.skip()
                frame = list()
            elif skip:
                plug.skip()
                frame = plug.finish()
            else:
                plug.process()
                frame = plug.finish()

        if not hard_skip:
            self._video_out.write(frame)

        if self._play:
            if self._player is None:
                self._player = plt.imshow(cv.cvtColor(frame, cv.COLOR_BGR2RGB))
            elif not hard_skip:
                self._player.set_data(cv.cvtColor(frame, cv.COLOR_BGR2RGB))
            if plt.waitforbuttonpress(timeout=.03):
                return False
            plt.draw()

        return True

    def run(self):
        """ Main loop for processing frame-by-frame from video source. """

        times = []
        if self._progress:
            for _i in tqdm(range(self._length)):
                start = time.time()
                processing = self.process_frame()
                end = time.time()
                times.append(end - start)
                if not processing:
                    break
        else:

            processing = True

            while processing:
                start = time.time()
                processing = self.process_frame()
                end = time.time()
                times.append(end - start)

        mean_time = statistics.mean(times[1:])

        # cv.destroyAllWindows()
        self._video_cap.release()
        self._video_out.release()

        print(mean_time)

    def _skip_memory_proc(self):
        """
        Determine whether or not the current frame should be
        skipped.
        """
        if self._skip_memory >= 1:
            self._skip_memory -= 1
            return False

        self._skip_memory += self._skip
        return True

    def _hard_skip_memory_proc(self):
        """
        Determine whether or not the current frame should be
        skipped.
        """
        if self._hard_skip_memory >= 1:
            self._hard_skip_memory -= 1
            return False

        self._hard_skip_memory += self._hard_skip
        return True



