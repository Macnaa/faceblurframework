
from collections import deque
import subprocess
import os
import os.path
import sys
import tempfile
import random
import string
import shutil

import cv2 as cv
import pandas as pd

from plugins.plugin import Plugin
from utility import flatten, stdchannel_redirected, face_embedded_rect, predict_object_movement
from blurring import blur_vertex_area


class Process_ofaceblur(Plugin):

    """
    Plugin for blurring faces in an image using
    Haar Cascade Classifiers (one for profile, one
    for front).

    When self._pass_blur is True the algorithm will
    blur the face and the same height below it. This
    is a hack to blur the name tags on keypasses.

    This class also includes a frame memory option which
    stores the last <self._memory> frame analyses as lists
    of face rectangles, and then blurs them all.


    """

    face_memory = deque()
    COLUMNS_0_16 = [" x_%s" % i for i in range(0, 17)]
    COLUMNS_26_22 = [" x_%s" % i for i in range(22, 27)]
    COLUMNS_26_22.reverse()
    COLUMNS_21_17 = [" x_%s" % i for i in range(17, 22)]
    COLUMNS_21_17.reverse()
    COLUMNS_FACE_ATTR_X = COLUMNS_0_16 + COLUMNS_26_22 + COLUMNS_21_17
    COLUMNS_FACE_ATTR_Y = [l.replace("x", "y") for l in COLUMNS_FACE_ATTR_X]

    EXE = os.environ["OPEN_FACE_LIB"] + "/build/bin/FaceLandmarkImg"
    # EXE = "../dat/openface/OpenFace/build/bin/FaceLandmarkImg"

    def __init__(self, file_name, memory, image):
        self._image = image
        self._faces = []
        self._return_image = None
        self._memory = memory

    def widen_face(self, face, scale_factor):
        C_x = sum([pair[0] for pair in face])/len(face)
        C_y = sum([pair[1] for pair in face])/len(face)

        wide_face = list()
        for x,y in face:
            new_x, new_y = (scale_factor*i - (scale_factor - 1)*C_i for i,C_i in ((x,C_x),(y,C_y)))
            # print("(x,y) = (%s,%s)\n\t\t(Cx,Cy) = (%2.f,%2.f)\n\t\t (nx,ny) = (%2.f,%2.f)" % (x,y,C_x,C_y,new_x,new_y))
            wide_face.append((new_x,new_y))

        return wide_face

    def call_openface(self, exe, img):

        faces = list()
        temp_dir = tempfile.gettempdir() + "/"
        tf_ext = ".jpg"
        while True:
            tfname = ''.join(random.choice(string.ascii_letters) for i in range(20))
            tf_path = temp_dir + tfname + tf_ext
            if not os.path.isfile(tf_path):
                break


        cv.imwrite(tf_path, img, [int(cv.IMWRITE_JPEG_QUALITY), 100])

        args = [exe, '-f', tf_path]
        cmd = " ".join(args)

        with open(os.devnull, 'w') as devnull:
            subprocess.call(cmd,shell=True,stdout=devnull)

        try:
            os.remove(tf_path)
        except:
            print("Unable to remove temporary file!")


        try:
            img_file = open("processed/%s.csv" % tfname)

            df = pd.read_csv(img_file)
            face_lists_x = df[Process_ofaceblur.COLUMNS_FACE_ATTR_X].values.tolist()
            face_lists_y = df[Process_ofaceblur.COLUMNS_FACE_ATTR_Y].values.tolist()

            for i in range(len(face_lists_x)):
                face_vertices = list(zip(face_lists_x[i], face_lists_y[i]))
                face_vertices= self.widen_face(face_vertices,1.5)
                faces.append(face_vertices)
        except:
            pass

        shutil.rmtree("processed")
        return faces

    def detect_faces(self):
        faces = self.call_openface(Process_ofaceblur.EXE, self._image)

        for face in faces:
            rect = face_embedded_rect(self._image, face,(2,3))

            if rect is not None:
                faces.remove(face)
                faces.append(rect)

        self._faces = faces

    def skip(self):
        if len(Process_ofaceblur.face_memory) > self._memory:
            Process_ofaceblur.face_memory.popleft()

        if self._memory > 0:
            old = Process_ofaceblur.face_memory.pop()
            Process_ofaceblur.face_memory.append(self._faces)
            Process_ofaceblur.face_memory.append(old)

    def process(self):
        self.detect_faces()
        if len(Process_ofaceblur.face_memory) > self._memory:
            Process_ofaceblur.face_memory.popleft()
        Process_ofaceblur.face_memory.append(self._faces)

    def finish(self):
        for face in flatten(Process_ofaceblur.face_memory):
            self._image = blur_vertex_area(face, self._image)

        return self._image

