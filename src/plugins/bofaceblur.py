
from collections import deque
from functools import reduce
import subprocess
import cv2 as cv
import pandas as pd
import numpy as np
import os
import sys
import os.path
import tempfile
import random
import string
import shutil
from plugins.plugin import Plugin
from utility import flatten, stdchannel_redirected, face_embedded_rect
from blurring import blur_vertex_area, blur_region


class Process_bofaceblur(Plugin):

    """
    Plugin for blurring faces in an image using
    Haar Cascade Classifiers (one for profile, one
    for front).

    When self._pass_blur is True the algorithm will
    blur the face and the same height below it. This
    is a hack to blur the name tags on keypasses.

    This class also includes a frame memory option which
    stores the last <self._memory> frame analyses as lists
    of face rectangles, and then blurs them all.


    """

    face_memory = deque()
    COLUMNS_0_16 = [" x_%s" % i for i in range(0, 17)]
    COLUMNS_26_22 = [" x_%s" % i for i in range(22, 27)]
    COLUMNS_26_22.reverse()
    COLUMNS_21_17 = [" x_%s" % i for i in range(17, 22)]
    COLUMNS_21_17.reverse()
    COLUMNS_FACE_ATTR_X = COLUMNS_0_16 + COLUMNS_26_22 + COLUMNS_21_17
    COLUMNS_FACE_ATTR_Y = [l.replace("x", "y") for l in COLUMNS_FACE_ATTR_X]


    FACES_PER_FRAME = deque()
    OPENFACE_CALLED = False

    def __init__(self, file_path, memory, image, multi_face=False, box_blur=False):
        self._image = image
        self._faces = []
        self._return_image = None
        self._memory = memory
        self._box_blur = box_blur

        if not multi_face:
            exe = os.environ['OPEN_FACE_LIB'] + "/build/bin/FeatureExtraction"
        else:
            exe = os.environ['OPEN_FACE_LIB'] + "/build/bin/FaceLandmarkVidMulti"

        if not Process_bofaceblur.OPENFACE_CALLED:
            Process_bofaceblur.FACES_PER_FRAME = self.call_openface_exe(exe, file_path)
            Process_bofaceblur.OPENFACE_CALLED = True

    @classmethod
    def multi_face(cls, file_path, memory, image):
        return cls(file_path, memory, image, multi_face=True)

    @classmethod
    def multi_face_box_blur(cls, file_path, memory, image):
        return cls(file_path, memory, image, multi_face=True, box_blur=True)

    @classmethod
    def box_blur(cls, file_path, memory, image):
        return cls(file_path, memory, image, multi_face=False, box_blur=True)

    def widen_face(self, face, scale_factor):
        C_x = sum([pair[0] for pair in face])/len(face)
        C_y = sum([pair[1] for pair in face])/len(face)

        wide_face = list()
        for x,y in face:
            new_x, new_y = (scale_factor*i - (scale_factor - 1)*C_i for i,C_i in ((x,C_x),(y,C_y)))
            # print("(x,y) = (%s,%s)\n\t\t(Cx,Cy) = (%2.f,%2.f)\n\t\t (nx,ny) = (%2.f,%2.f)" % (x,y,C_x,C_y,new_x,new_y))
            wide_face.append((new_x,new_y))

        return wide_face

    def call_openface_exe(self, exe, file_path):

        faces_frame_list = deque()
        args = [exe, '-f', file_path]
        cmd = " ".join(args)

        with open(os.devnull, 'w') as devnull:
            subprocess.call(cmd,shell=True, stdout=devnull)

        file_prefix, file_ext = os.path.splitext(file_path)
        file_name = file_prefix.split('/')[-1]
        with open("processed/%s.csv" % file_name) as csv_file:

            df = pd.read_csv(csv_file)

            count = 0
            for i in df['frame'].unique():
                count += 1
                # print("i: %s, count: %s" % (i, count))

                assert(count <= i)

                if i > count:
                    steps = i - count

                    for _j in range(steps):
                        # print(count)
                        faces_frame_list.append([])
                        count += 1

                faces = list()
                face_rows = df[df.frame == i]

                face_lists_x = face_rows[Process_bofaceblur.COLUMNS_FACE_ATTR_X].values.tolist()
                face_lists_y = face_rows[Process_bofaceblur.COLUMNS_FACE_ATTR_Y].values.tolist()

                for i in range(len(face_lists_x)):
                    face_vertices = list(zip(face_lists_x[i], face_lists_y[i]))
                    face_vertices = self.widen_face(face_vertices,1.5)
                    faces.append(face_vertices)

                faces_frame_list.append(faces)



        # print("Frames according to face_frame_list: %s" % str( len(faces_frame_list) ))
        # print("Frames according to csv: %s" % len(df.groupby('frame')))
        shutil.rmtree("processed")
        return faces_frame_list

    def detect_faces(self):
        faces = Process_bofaceblur.FACES_PER_FRAME.popleft()

        for face in faces:
            rect = face_embedded_rect(self._image, face,(2,3))

            if rect is not None:
                faces.remove(face)
                faces.append(rect)

        self._faces = faces

    def face2box(self, face):
        face_array = np.array([[round(f), round(g)] for f, g in face])
        box = cv.boundingRect(face_array)
        return box

    def skip(self):

        out_of_fuel = Process_bofaceblur.OPENFACE_CALLED and not Process_bofaceblur.FACES_PER_FRAME

        if not out_of_fuel:
            self._faces = Process_bofaceblur.FACES_PER_FRAME.popleft()
        else:
            self._faces = []

        if len(Process_bofaceblur.face_memory) > self._memory:
            Process_bofaceblur.face_memory.popleft()

        Process_bofaceblur.face_memory.append(self._faces)

    def process(self):

        out_of_fuel = Process_bofaceblur.OPENFACE_CALLED and not Process_bofaceblur.FACES_PER_FRAME

        if not out_of_fuel:
            self.detect_faces()
        else:
            self._faces = []

        if len(Process_bofaceblur.face_memory) > self._memory:
            Process_bofaceblur.face_memory.popleft()

        Process_bofaceblur.face_memory.append(self._faces)

    def finish(self):

        if self._box_blur:
            faces2blur = [self.face2box(f) for f in flatten(Process_bofaceblur.face_memory)]
            self._return_image = blur_region(faces2blur, self._image)
        else:
            faces2blur = flatten(Process_bofaceblur.face_memory)
            for face in faces2blur:
                self._image = blur_vertex_area(face, self._image)
            self._return_image = self._image

        return self._return_image

