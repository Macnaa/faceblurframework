import subprocess
import pandas as pd
import cv2 as cv

COLUMNS_0_16 = ["X_%s" % i for i in range(0, 17)]
COLUMNS_26_22 = ["X_%s" % i for i in range(22, 27)].reverse()
COLUMNS_21_17 = ["X_%s" % i for i in range(17, 22)].reverse()
COLUMNS_FACE_ATTR = COLUMNS_0_16 + COLUMNS_26_22 + COLUMNS_21_17
EXE = "/opt/openface/OpenFace/build/bin/FaceLandmarkImg"

def call_openface(exe, img):

    args = [exe, '-f', img]
    subprocess.call(args)

    with open("processed/%s.csv" % img.split(".")[0]) as img_file:

        df = pd.read_csv(img_file)
        face_lists = df[COLUMNS_FACE_ATTR].values.tolist()

        return face_lists






