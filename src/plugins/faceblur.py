
import cv2 as cv
from plugins.plugin import Plugin
from utility import flatten
from blurring import blur_region
from collections import deque

class Process_faceblur(Plugin):

    """
    Plugin for blurring faces in an image using
    Haar Cascade Classifiers (one for profile, one
    for front).

    When self._pass_blur is True the algorithm will
    blur the face and the same height below it. This
    is a hack to blur the name tags on keypasses.

    This class also includes a frame memory option which
    stores the last <self._memory> frame analyses as lists
    of face rectangles, and then blurs them all.


    """

    face_memory = deque()

    def __init__(self, pass_blur, file_name, memory, image):
        self._image = image
        self._faces = []
        self._return_image = None
        self._pass_blur = pass_blur
        self._memory = memory

    def elon_faces(self, faces):
        elon_new_faces = []

        for f in faces:
            x,y,w,h = f
            elon_new_faces.append((x,y,w,2*h))

        return elon_new_faces

    def detect_eyes(self, gray, face_cascade):
        haar_right_eye = "../dat/face_detect/haarcascade_righteye_2splits.xml"
        haar_left_eye = "../dat/face_detect/haarcascade_lefteye_2splits.xml"
        face_cascade.load(haar_left_eye)
        left_eyes = face_cascade.detectMultiScale(gray,
                scaleFactor=1.2,
                minNeighbors=5,
                flags=cv.CASCADE_SCALE_IMAGE)
        face_cascade.load(haar_right_eye)
        right_eyes = face_cascade.detectMultiScale(gray,
                scaleFactor=1.2,
                minNeighbors=2,
                flags=cv.CASCADE_SCALE_IMAGE)

        new_rects = list()
        for i in left_eyes:
            x,y,w,h = i
            new_rects.append((x - 2*w, y - h, 3*w, 4*h))

        for i in right_eyes:
            x,y,w,h = i
            new_rects.append((x, y - h, 3*w, 4*h))

        return new_rects

    def detect_faces(self):
        gray = cv.cvtColor(self._image, cv.COLOR_BGR2GRAY)

        haar_front_alt = "../dat/face_detect/haarcascade_frontalface_alt.xml"
        haar_profile = "../dat/face_detect/haarcascade_profileface.xml"
        # lbp_profile = "../dat/face_detect/lbpcascade_profileface.xml"
        # lbp_front = "../dat/face_detect/lbpcascade_frontalface_improved.xml"

        face_cascade = cv.CascadeClassifier()
        for i in (haar_profile, haar_front_alt):
            face_cascade.load(i)
            new_faces = face_cascade.detectMultiScale(gray,
                    scaleFactor=1.1,
                    minNeighbors=5,
                    minSize=(30,30),
                    flags=cv.CASCADE_SCALE_IMAGE)

            if self._pass_blur:
                new_faces = self.elon_faces(new_faces)


            self._faces.extend(new_faces)

        self._faces.extend(self.detect_eyes(gray, face_cascade))

    def skip(self):
        if len(Process_faceblur.face_memory) > self._memory:
            Process_faceblur.face_memory.popleft()

        if self._memory > 0:
            old = Process_faceblur.face_memory.pop()
            Process_faceblur.face_memory.append(self._faces)
            Process_faceblur.face_memory.append(old)
        return self.finish()

    def process(self):
        self.detect_faces()
        if len(Process_faceblur.face_memory) > self._memory:
            Process_faceblur.face_memory.popleft()

        Process_faceblur.face_memory.append(self._faces)
        return self.finish()

    def finish(self):

        self._return_image = blur_region(flatten(Process_faceblur.face_memory), self._image)

        return self._return_image


