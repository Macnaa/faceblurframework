
from abc import ABC, abstractmethod

class Plugin(ABC):

    @abstractmethod
    def __init__(self, image):
        self.image = image

    @abstractmethod
    def process(self):
        pass

    @abstractmethod
    def skip(self):
        pass

    @abstractmethod
    def finish(self):
        pass
