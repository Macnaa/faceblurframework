
import cv2 as cv
from plugins.plugin import Plugin
from collections import deque
from utility import blur_region, flatten
from matplotlib import pyplot as plt

class Process_npblur(Plugin):

    rec_memory = deque()

    def __init__(self, file_name, frame_memory, image):
        self._image = image
        self._recs = []
        self._return_image = None
        self._memory = frame_memory


    def detect_plates(self):
        gray = cv.cvtColor(self._image, cv.COLOR_BGR2GRAY)
        lbp_plate = "../dat/np_detect/lbp_cascade_au_plates.xml"

        plates_cascade = cv.CascadeClassifier()
        for i in [lbp_plate]:
            plates_cascade.load(i)
            new_plates = plates_cascade.detectMultiScale(gray,
                    scaleFactor=1.2,
                    minNeighbors=2,
                    flags=cv.CASCADE_SCALE_IMAGE)

            self._recs.extend(new_plates)

    def skip(self):
        if len(Process_npblur.rec_memory) > self._memory:
            Process_npblur.rec_memory.popleft()

        Process_npblur.rec_memory.append(self._recs)

    def process(self):
        self.detect_plates()
        if len(Process_npblur.rec_memory) > self._memory:
            Process_npblur.rec_memory.popleft()

        Process_npblur.rec_memory.append(self._recs)

    def finish(self):
        self._return_image = blur_region(flatten(Process_npblur.rec_memory), self._image)

        return self._return_image


