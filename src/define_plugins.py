from functools import partial

# To install plugin import pugin in the import list below
# and then add it to the protocol list, this will be the
# key will be the command you add to the command line.
# NOTE: Make sure the class constructor value you have
# listed takes only the image as input, if necessary use
# partial function application.

# Plugin Import
from plugins import faceblur
from plugins import npblur
from plugins import ofaceblur
from plugins import bofaceblur

# Protocol mapping
protocols = {
    'fpblur': partial(faceblur.Process_faceblur, True),
    'npblur': npblur.Process_npblur,
    'faceblur': partial(faceblur.Process_faceblur, False),
    'ofaceblur': ofaceblur.Process_ofaceblur,
    'bofaceblur': bofaceblur.Process_bofaceblur,
    'bofaceblur-box': bofaceblur.Process_bofaceblur.box_blur,
    'bofaceblur-multi': bofaceblur.Process_bofaceblur.multi_face,
    'bofaceblur-multi-box': bofaceblur.Process_bofaceblur.multi_face_box_blur,
}
