
import sys
import os
from random import randint
from contextlib import contextmanager,redirect_stderr,redirect_stdout
from collections import deque
import statistics as stat
import copy

# import execnet
import cv2 as cv
import numpy as np
import scipy.stats as st
from matplotlib import pyplot as plt

def centroid(ls):
    """ Returns centroid for list of tuples. """
    return (stat.mean(i[0] for i in ls), stat.mean(i[1] for i in ls))

# def call_python_version(Version, Module, Function, ArgumentList):
    # gw      = execnet.makegateway("popen//python=python%s" % Version)
    # channel = gw.remote_exec("""
        # from %s import %s as the_function
        # channel.send(the_function(*channel.receive()))
    # """ % (Module, Function))
    # channel.send(ArgumentList)
    # return channel.receive()

# stdchannel: Channel to redirect to
# dest_filename: Filename to redirect channel to
@contextmanager
def stdchannel_redirected(stdchannel, dest_filename):
    oldstdchannel = None
    dest_file = None
    try:
        oldstdchannel = os.dup(stdchannel.fileno())
        dest_file = open(dest_filename, 'w')
        os.dup2(dest_file.fileno(), stdchannel.fileno())

        yield
    finally:
        if oldstdchannel is not None:
            os.dup2(oldstdchannel, stdchannel.fileno())
        if dest_file is not None:
            dest_file.close()

# Function: Plays a video and asks user for replays
# video: list of frames
# title: title of video window
# framerate: rate to play frames at
def video_player(video, title, framerate):

    cv.namedWindow(title,cv.WINDOW_NORMAL)

    play = 1
    while(play):
        print(play)
        for frame in video:

            cv.imshow(title, frame)

            # Use & 0xff to remove all but the 8 necessary bits
            k = cv.waitKey(framerate) & 0xff

            # 27 represents the ESC key
            if k == 27:
                break

        if play == 1:
            ans = input("Replay? ([y]/n) ")

            if ans.isnumeric():
                play += int(ans) - 1
            if ans != "n":
                play += 1

        play -= 1

    cv.destroyAllWindows()

# Function: Blurs a set of rectangles on an image
# rectangles: List of rectangles <x,y,w,h>
# image: image to be blurred
def blur_region(rectangles, image):

    return_image = image.copy()

    for f in rectangles:
        x,y,w,h = f
        sub_face = image[y:y+h, x:x+w]
        # sub_face = cv.GaussianBlur(sub_face, (23,23), 30)
        sub_face = cv.blur(sub_face, (35,35))
        return_image[y:y+sub_face.shape[0], x:x+sub_face.shape[1]] = sub_face

    return return_image

# Function: Flattens list by one step
# l: list to be flattened
# Return: Flattened list
flatten = lambda l: [item for sublist in l for item in sublist]


def bounding_rectangle_detection(img, verbose=False):

    padd = 5

    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    ret, thresh = cv.threshold(gray, 75, 255, 1)

    mode = st.mode(st.mode(thresh).mode[0]).mode[0]
    thresh_expanded = np.pad(thresh.copy(), ((padd,padd),(padd,padd)), 'constant', constant_values=mode)
    contours, h = cv.findContours(thresh_expanded, 1, 2)

    rects = []

    # for cnt in contours:

        # shape = cv.approxPolyDP(cnt, 0.01*cv.arcLength(cnt, True), True)

        # if len(shape) == 4:

            # rects.append(cnt)

    # The second biggest is chosen because the largest includes the entire img
    sorted_cnts = sorted(contours, key = cv.contourArea, reverse = True)

    img_y, img_x, _ = img.shape

    sorted_unpadded_cnts = []
    for s in sorted_cnts:
        # print("%s, %s, %s, ==  %s" % (img_y - 3, img_x - 3, ( img_x - 3 )*( img_y - 3), cv.contourArea(s)))
        unpadded_s = np.array([np.array([[f[0][0] - padd, f[0][1] - padd]]) for f in s])
        if ( img_x - 3 )*( img_y - 3) >= cv.contourArea(s):
            sorted_unpadded_cnts.append(unpadded_s)


    if verbose:
        plt.subplot(121), plt.imshow(thresh_expanded)

        img_c = thresh_expanded.copy()
        for cnt in sorted_cnts:
            color = (randint(0,255), randint(0,255), randint(0,255))
            cv.drawContours(img_c,[cnt], 0, color, 2)

        plt.subplot(122), plt.imshow(img_c)

        plt.show()
        input()

    if sorted_unpadded_cnts:
        return sorted_unpadded_cnts[0]
    else:
        return None

def expand_rectangle(Xs, Ys, multiplier, bounds=None):
    (x0, x1) = Xs
    (y0, y1) = Ys
    (d0, d1) = multiplier

    if bounds is None:
        bounds = ((-math.inf, math.inf), (-math.inf, math.inf))

    (b00, b01), (b10, b11) = bounds

    x_min = int( x0 - d0*(x1 - x0) if x0 - d0*(x1 - x0) > b00 else b00 )
    y_min = int( y0 - d1*(y1 - y0) if y0 - d1*(y1 - y0) > b10 else b10 )

    x_max = int( x1 + d0*(x1 - x0) if x1 + d0*(x1 - x0) < b01 else b01 )
    y_max = int( y1 + d1*(y1 - y0) if y1 + d1*(y1 - y0) < b11 else b11)

    return ((x_min, x_max), (y_min, y_max))

def face_in_rect(rect, face):
    return all( cv.pointPolygonTest(rect, f, True) >= 0 for f in face )

def face_embedded_rect(img, face, dim):

    x1 = max(f[1] for f in face)
    x0 = min(f[1] for f in face)
    y1 = max(f[0] for f in face)
    y0 = min(f[0] for f in face)

    shape = expand_rectangle((x0, x1), (y0, y1), dim, ((0, img.shape[0]), (0, img.shape[1])))


    cropped_img = img[shape[1][0]:shape[1][1], shape[0][0]:shape[0][1]]
    cropped_face = [(f[0] - shape[0][0], f[1] - shape[1][0]) for f in face]

    try:
        rect_cont = bounding_rectangle_detection(cropped_img)
    except Exception as e:
        return None

    if rect_cont is None:
        return None

    if face_in_rect(rect_cont, cropped_face):
        uncropped_rect = [(f[0][0] + shape[0][0], f[0][1] + shape[1][0]) for f in rect_cont]
        return uncropped_rect
    else:
        # return None

        # uncropped_rect = np.array([np.array([[f[0][0] + shape[0][0], f[0][1] + shape[1][0]]]) for f in rect_cont])
        # cv.drawContours(img,[uncropped_rect], 0, (255, 0, 255), 3)
        # plt.imshow(img)
        # plt.show()
        # input()
        return None

def greedy_match(list1, list2, f):

    # l1 = copy.deepcopy(list1)
    # l2 = copy.deepcopy(list2)
    l1 = list1
    l2 = list2

    dist_list = sorted( [(e1, e2, f(e1, e2)) for e1 in l1 for e2 in l2], key=lambda l: l[2])

    matches = []

    used_l1 = []
    used_l2 = []
    for (e1, e2, dist) in dist_list:

        if ( e1 not in used_l1 ) and ( e2 not in used_l2 ):

            matches.append((e1, e2))
            used_l1.append(e1)
            used_l2.append(e2)


    # del l1
    # del l2

    return matches

def centroid_distance(f1, f2):
    c1 = centroid(f1)
    c2 = centroid(f2)

    v = (c2[0] - c1[0], c2[1] - c1[1])

    return np.linalg.norm(v)

def predict_object_movement(q):

    if not q:
        return None
    elif len(q) == 1:
        return q[0]

    matches = greedy_match(q[-1], q[-2], centroid_distance)

    new_objects = []

    for o1, o2 in matches:
        c1 = centroid(o1)
        c2 = centroid(o2)
        v = (c2[0] - c1[0], c2[1] - c1[1])

        o3 = [(ox + v[0], oy + v[1]) for ox, oy in o2]

        new_objects.append(o3)

    return new_objects


















