
"""
This is an implementation of the convex polyhedral
intersection algorithm published in:

Barba, L., & Langerman, S. (2014, December). Optimal
detection of intersections between convex polyhedra.
In Proceedings of the twenty-sixth annual ACM-SIAM
symposium on Discrete Algorithms (pp. 1641-1654).
Society for Industrial and Applied Mathematics.

Author: Aleck MacNally
Organization: DSTO
"""

#pylint: disable=invalid-name

import math
import operator
import statistics as st

import numpy as np
import scipy.spatial

def centroid(A):
    """
    Returns the centroid for a numpy.array, A,
    of the following form:
    numpy.ndarray([
    [1, 2],
    [3, 4]
    ])
    """
    return np.array([st.mean(i[0] for i in A), st.mean(i[1] for i in A)])

def sort_clockwise(P):
    """
    Sorts a polygon, P, so that its vertices are in
    clockwise order (assumes convexity).

    P is a numpy.array of points eg.
    numpy.ndarray([
    [1, 2],
    [3, 4]
    ])

    Returns sorted Array.
    """

    X, Y = centroid(P)
    angles = dict()
    for i, v in enumerate(P):
        x, y = v
        angles[i] = math.atan2(x - X, y - Y) + math.pi*2

    sorted_angles = sorted(angles.items(), key=operator.itemgetter(1))
    cw_sorted_angles = [P[i] for i in [a[0] for a in sorted_angles]]

    return cw_sorted_angles

def edges(Q):
    """
    Returns the edges of a sorted convex polygon, Q,
    in the form of a numpy.array.
    """

    # Our polygon must have at least 2 points
    if len(Q) < 2:
        return np.array([])

    e_0 = None
    edges = []
    for e_1 in Q:
        if e_0 is not None:
            edges.append([e_0, e_1])
        e_0 = e_1

    edges.append([Q[-1], Q[0]])

    return np.array(edges)


def convex_polygon_intersect(P, Q):
    """
    P, Q are sorted convex polygons discribed by points
    in a numpy.array, e.g:
    numpy.ndarray([
    [1, 2],
    [3, 4]
    ])

    P, Q must be sorted in clockwise fashion, which may be done
    using the 'sort_clockwise' function in the same module.

    Return is a boolean value indicating whether P and Q
    intersect.
    """

    Q_edges = edges(Q)

    T_Q = _edge_hull(_triangle_choice(Q_edges))
    T_P = _triangle_choice(P)

    t_intersect = triangle_intersect_vertices(T_Q, T_P)

    if t_intersect:
        _intersection_invariant(T_Q, T_P)
    else:
        _separation_invarient(T_Q, T_P)





def _separation_invarient():
    pass

def _intersection_invariant(Q, Q_edges, P, T_Q, T_P):
    tqs = _split_tq(T_Q)



def convex_polyhedra_intersect(P, Q):
    pass

def _triangle_choice(S):
    """
    Private function:
    Returns evenly spaced triangle from given numpy.array.
    """
    chain_size = math.ceil( ( len(S) - 3 ) / 3 )
    T = S[0::chain_size + 1]

    return T

def _edge_hull(Edges):
    """
    Private Function:
    Returns the Edge hull of Edges.
    """
    lines = []
    for edge in Edges:
        v1, v2 = edge
        lines.append(_lineq(v1, v2))

    l_0 = None
    vertices = []
    for l_1 in lines:
        if l_0 is not None:
            vertex = _lineq_intersect(l_0, l_1)
            vertices.append(vertex)
        l_0 = l_1

    vertices.append(_lineq_intersect(lines[-1], lines[0]))

    return vertices

def _lineq(p1, p2):
    """
    Private Function:
    Linear Eqation of two points,
    https://stackoverflow.com/questions/20677795/
    how-do-i-compute-the-intersection-point-of-two-lines-in-python
    Author: rook
    """
    A = (p1[1] - p2[1])
    B = (p2[0] - p1[0])
    C = (p1[0]*p2[1] - p2[0]*p1[1])
    return A, B, -C

def _lineq_intersect(L1, L2):
    """
    Private Function:
    Intersection Point of two lines,
    https://stackoverflow.com/questions/20677795/
    how-do-i-compute-the-intersection-point-of-two-lines-in-python
    Author: rook
    """
    D  = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x, y
    else:
        return None

def triangle_intersect(T1, T2):
    """
    Returns whether two triangles intersect.
    Where T1, T2 are three edges.
    """
    for t1 in T1:
        line_t1 = _lineq(*t1)
        for t2 in T2:
            line_t2 = _lineq(*t2)
            inter = _lineq_intersect(line_t1, line_t2)

            if inter is None:
                continue
            else:
                on_t1_x = t1[0][0] <= inter[0] <= t1[1][0] or \
                        t1[1][0] <= inter[0] <= t1[0][0]
                on_t1_y = t1[0][1] <= inter[1] <= t1[1][1] or \
                        t1[1][1] <= inter[1] <= t1[0][1]
                on_t2_x = t2[0][0] <= inter[0] <= t2[1][0] or \
                        t2[1][0] <= inter[0] <= t2[0][0]
                on_t2_y = t2[0][1] <= inter[1] <= t2[1][1] or \
                        t2[1][1] <= inter[1] <= t2[0][1]

                if on_t1_x and on_t1_y and on_t2_x and on_t2_y:
                    return True
    return False

def triangle_intersect_vertices(T1, T2):
    """
    Wrapper function for triangle_intersect for Triangles
    represented as vertices.
    """
    T1_e = edges(T1)
    T2_e = edges(T2)
    return triangle_intersect(T1_e, T2_e)










