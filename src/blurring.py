
import math
import cv2 as cv
import numpy as np
import operator
from functools import partial
from random import randint
from utility import flatten

# Function: Indicates whether a point is within the bouds of a rectangle
def is_in_rect(point, rect):
    x,y,w,h = rect
    u,v = point

    return True if u > x and u < x + w and v > y and v < y + h else False

# Function: Translates offset rectangle representation to point based
def rect_to_vertices(rect):
    x,y,w,h = rect
    return [(x,y),(x+w,y),(x+w,y+h),(x,y+h)]

# Function: Translates a set of overlapping rectangles into a polygon with
# the output border vertices
def rect_to_polygon(rectangles):
    vertices = flatten(list(map(rect_to_vertices, rectangles)))
    keep_vertices = []

    for v in vertices:
        if not (True in list(map(partial(is_in_rect, v), rectangles))):
            keep_vertices.append(v)

    return keep_vertices

# Function: Finds centroid of the set of points vs
def find_centroid(vs):
    x,y = 0,0

    for v in vs:
        x += v[0]
        y += v[1]

    return (x / len(vs), y / len(vs))

# Function: Sorts a list of vertices, vs, in clockword order
def sort_vertices(vs):
    (X,Y) = find_centroid(vs)
    angles = dict()
    for i in range(0, len(vs)):
        x,y = vs[i]
        angles[i] = math.atan2(x - X, y - Y) + math.pi*2

    sorted_angles = sorted(angles.items(), key=operator.itemgetter(1))
    new_vs = [vs[i] for i in [a[0] for a in sorted_angles]]

    return new_vs

# Function: Indicates whether two rectangles intersect
def rect_intersect(r1,r2):
    x,y,w,h = r1

    intersect = False
    for i in [(x,y),(x+w,y),(x,y+h),(x+w,y+h)]:
        if is_in_rect(i,r2): intersect = True

    return intersect

def blur_vertex_area(vertices, image):
    roi_corners = np.array([vertices],dtype = np.int32)
    blurred_image = cv.GaussianBlur(image,(43, 43), 30)
    mask = np.zeros(image.shape, dtype=np.uint8)
    channel_count = image.shape[2]
    ignore_mask_color = (255,)*channel_count
    cv.fillPoly(mask, roi_corners, ignore_mask_color)
    mask_inverse = np.ones(mask.shape).astype(np.uint8)*255 - mask
    final_image = cv.bitwise_and(blurred_image, mask) + cv.bitwise_and(image, mask_inverse)
    # final_image = image.copy()
    # cv.drawContours(final_image, roi_corners, -1, (randint(0,255), randint(0,255), randint(0,255)),3)

    return final_image

# Function: Blurs a list of rectangles as if the external edges were the
# boundary of a polygon
def blur_contiguous_region(rectangles, image):
    vertices = sort_vertices(rect_to_polygon(rectangles))

    final_image = blur_vertex_area(vertices,image)

    return final_image

# Function: Separates rects into sets of overlapping rectangles
def overlapping_regions(rects):
    regions = []
    rectangles = lrects_to_srects(rects)

    for r in rectangles:
        ints = list(map(partial(rect_intersect,r), rectangles))
        true_ints = [i for i in range(0, len(ints)) if ints[i]]

        for r_idx in true_ints:
            added = False
            for i in range(0,len(regions)):
                if rectangles[r_idx] in regions[i]:
                    regions[i].add(r)
                    added = True
                elif r in regions[i]:
                    regions[i].add(rectangles[r_idx])
                    added = True
            if not added:
                regions.append(set([r, rectangles[r_idx]]))

        if not true_ints:
            regions.append(set([r]))

    return list(map(list,regions))


# Translates rectangles between lists and tuples
def lrects_to_srects(rectangles):
    return list(tuple(i) for i in rectangles)

def srects_to_lrects(rectangles):
    return list(list(i) for i in rectangles)

# Function: Entry function. Takes a list of rectangles and
# blurs the region encompassed by them
def blur_region(rectangles, image):
    if not rectangles :
        return image

    regions = overlapping_regions(rectangles)

    final_image = image
    for rs in regions:
        final_image = blur_contiguous_region(rs,final_image)

    return final_image


