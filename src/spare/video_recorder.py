
# --- UNCLASSIFIED ---

from __future__ import print_function, division
import threading
import time
import subprocess
import os
import cv2 as cv
import numpy as np

class VideoRecorder():
    "Video class based on openCV"
    def __init__(self, plugins, play = True, memory = 0, skip = 0, name="temp_video.avi", fourcc="MJPG", sizex=640, sizey=480, camindex=0, fps=30):
        self.open = True
        self.device_index = camindex
        self.fps = fps                  # fps should be the minimum constant rate at which the camera can
        self.fourcc = fourcc            # capture images (with no decrease in speed over time; testing is required)
        self.frameSize = (sizex, sizey) # video formats and sizes also depend and vary according to the camera used
        self.video_filename = name
        self.video_cap = cv.VideoCapture(self.device_index)

        ret,frame = self.video_cap.read()
        print(ret)
        if ret:
            sizex,sizey = frame.size()
            print(frame.size())

        self.video_writer = cv.VideoWriter_fourcc(*self.fourcc)
        self.video_out = cv.VideoWriter(self.video_filename, self.video_writer, self.fps, self.frameSize)
        self.frame_counts = 1
        self.start_time = time.time()
        self._memory = memory
        self._skip = skip
        self._plugins = plugins
        self._play = play

    def record(self):
        "Video starts being recorded"
        # counter = 1
        timer_start = time.time()
        timer_current = 0

        while self.open:
            ret, frame = self.video_cap.read()

            if ret:

                for plugin in self._plugins:
                    plug = plugin(self._memory,self._skip, frame)
                    frame = plug.process()

                # frames.append(frame)

                self.video_out.write(frame)
                self.frame_counts += 1
                time.sleep(1/self.fps)

                if self._play:
                    cv.namedWindow("Video Processing",cv.WINDOW_NORMAL)
                    cv.imshow("Video Processing",frame)
                    k = cv.waitKey(30) & 0xff
                    if k == 27:
                        break
            else:
                break

    def stop(self):
        "Finishes the video recording therefore the thread too"
        if self.open:
            self.open=False
            self.video_out.release()
            self.video_cap.release()
            cv.destroyAllWindows()

    def start(self):
        "Launches the video recording function using a thread"
        video_thread = threading.Thread(target=self.record)
        video_thread.start()
