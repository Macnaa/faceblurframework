#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Program for processing a video image by image and applying
selected plugins.
"""

import sys
import os

import argparse
import cv2

import define_plugins
from processor import Processor
from utility import stdchannel_redirected

def argument_parser():
    """
    Function: wraps argument parser
    Return Parser
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--index", type=int,
                        help="Use camera with supplied index.")
    parser.add_argument("-f", "--file", type=str,
                        help="Use supplied file.")
    parser.add_argument("-n", "--name", type=str,
                        help="What name under which to store the processed file.")
    parser.add_argument("-c", "--camera", action="store_true",
                        help="Choose from list of camera indices")
    parser.add_argument("-p", "--plugins", nargs='+',
                        help="List required processes to run..")
    parser.add_argument("-m", "--memory", type=int,
                        help="Indicates number of frames to be stored for \
                        analysis")
    parser.add_argument("-s", "--skip", type=float,
                        help="Indicates the number of frames to skip proccesing on. Example: 0.25\
                        will skip every fourth frame, whereas 4 will process every fourth frame.\
                        It is important that you consider how frame memory interacts \
                        with this value.")
    return parser

def get_working_cam_idx():
    """
    Function: Retrieves a list of working camera indices
    Return: Int List
    """
    idx = 0
    working_idx = []

    while True:

        # Suppresses warning output from opencv
        # as we are looking for the point at which this fails
        with stdchannel_redirected(sys.stderr, os.devnull):
            cap = cv2.VideoCapture(idx)

        read_cap = cap.read()[0]

        if not read_cap:
            break
        else:
            working_idx.append(idx)

        cap.release()
        idx += 1

    return working_idx

def main():
    """Entry Point into program"""

    # pylint: disable=too-many-locals
    # pylint: disable=too-many-branches
    # pylint: disable=too-many-statements

    parser = argument_parser()
    args = parser.parse_args()

    # Video source file name or camera index
    file_name = None
    output_file_name = None
    idx = None

    # List of plugins (processes) to run on video
    p_list = []
    proc = None

    # Plays video after processing
    play = True
    frame_memory = None
    frame_skip = None

    # Sets frame memory
    if args.memory is not None:
        frame_memory = args.memory
    else:
        frame_memory = 0

    # Sets frame skip
    if args.skip is not None:
        frame_skip = args.skip
    else:
        frame_skip = 0

    if args.name is None:
        name = "../output/processed_video"
    else:
        name = args.name

    ext = ".avi"
    name = "/" + name + "_"
    local_path = os.getcwd()
    count = 0

    while True:
        if os.path.exists(str(local_path) + name + str(count) + ext):
            count += 1
        else:
            output_file_name = str(local_path) + name + str(count) + ext
            break

    # Retrieve Plugins from command line
    if args.plugins is not None:
        for plug_name in args.plugins:
            try:
                # Get plugin definitions
                plug = define_plugins.PROTOCOLS[plug_name]
            except KeyError as err:
                print("Process Error: " + str(err) + " does not exist.")
                continue

            p_list.append(plug)

    # Determine video source and create video processor
    if args.file is not None:
        file_name = args.file
        proc = Processor.process_file(file_name, output_file_name, play,
                                      frame_memory, frame_skip, plugins=p_list)
    else:
        idx = None
        if args.index is not None:
            idx = args.index

        # Asks user to choose camera from list of cameras
        elif args.camera:
            working_idx = get_working_cam_idx()

            if not working_idx:
                print("No camera detected!")
                exit(0)

            for p_idx in working_idx:
                print(str(p_idx) +".\n")

            chosen_idx = None
            while chosen_idx is None:
                print("Please enter desired camera index:")
                p_idx = input()

                if int(p_idx) in working_idx:
                    chosen_idx = p_idx

            idx = chosen_idx
        else:
            print("Please provide a video source!")
            exit(0)

        proc = Processor.process_camera(int(idx), output_file_name, play, frame_memory,
                                        frame_skip, plugins=p_list)
    proc.run()


    # Resultant video
    exit(0)


if __name__ == "__main__":
    main()
